#!/usr/bin/env python

import pandas as pd
import numpy as np
from scipy import signal
from scipy.integrate import simps
from scipy.signal import resample
from statsmodels.nonparametric.smoothers_lowess import lowess
from find_cutoff import*
from speed_from_accelerometer import setup_data


def main(filename):
    data = setup_data(filename)
    resampled = data.resample(".25S").mean()
    low_pass_acc = lowess(resampled['total'], resampled.index, frac=.001)
    median = np.median(low_pass_acc[:,1])
    steps = low_pass_acc[np.isclose(low_pass_acc, median, rtol=median*.1)].shape[0]
    print('\nSteps:', int(steps/2))


if __name__ == '__main__':
    import sys
    argc = len(sys.argv)
    if argc == 2:
        main(sys.argv[1])
    else:
        print("""Usage:
    data file: name of csv file containing your walking data 
               must have time, x, y, z columns
    """)

