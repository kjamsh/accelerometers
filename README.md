# Playing with Accelerometers

This python app will use phone accelerometer data as input to provide an estimate for the phone user’s speed, distance travelled, and number of steps taken.  
It can be used for applications such as fitness tracking, navigation, and games.

The requirements for this app are SciPy, NumPy, StatsModels, and Pandas.

There are two scripts “speed_from_accelerometer.py” and “pedometer.py” that both take a .csv file as input and print their results to STDOUT.

"speed_from_accelerometer.py" will calculate and display the speed and distance. Check usage by running the script without arguments.

    $ python speed_from_accelerometer.py android_datasets/100pocket-lac1.csv loess

"pedometer.py" will calculate and display an estimated number of steps over the trip.

    $ python pedometer.py android_datasets/100pocket-lac1.csv

Jupyter notebooks Approximations.ipynb and speed-from-acceleration.ipynb explain the data filtering and calculation methodology, as well as our error rates.

"android_datasets" and "iphone_datasets" folders contain sample input files. 
The apps used to collect the data were Physics Toolbox Sensor Suite for Android and Sensor Kinetics Pro for iPhone.